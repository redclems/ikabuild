package fr.ikalogique.ikalist.model;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListChampion {
    ArrayList<Champion> mListe;

    public ListChampion()
    {
        mListe = new ArrayList<>();

        mListe.add(new Champion(1, "Ryze"));
        mListe.add(new Champion(2, "Nasus"));
        mListe.add(new Champion(3, "Lysandra"));
    }


    public Champion get(int i) {
        return mListe.get(i);
    }

    public int size() {
        return mListe.size();
    }
}
