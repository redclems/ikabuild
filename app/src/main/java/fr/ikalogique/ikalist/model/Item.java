package fr.ikalogique.ikalist.model;

public class Item {

    private int id;
    private String name;
    private String pathImg;

    public Item(int id, String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPathImg() {
        return pathImg;
    }
}
