package fr.ikalogique.ikalist.model;

import java.util.ArrayList;
import java.util.List;

public class Build {


    private int id;
    private String name;
    private Champion champ;
    private List<Item> listItem;

    public Build(int id, String name, Champion champ) {
        this.name = name;
        this.id = id;
        this.champ = champ;
        this.listItem = new ArrayList<>();
    }

    public Build(int id, String name, Champion champ, List<Item> listItem) {
        this.name = name;
        this.id = id;
        this.champ = champ;
        this.listItem = listItem;
    }

    public Build(int id, String name) {
        this.name = name;
        this.id = id;
        this.listItem = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public boolean addItem(Item item){
        if( this.listItem.size() < 6) {
            this.listItem.add(item);
            return true;
        }
        return false;
    }

    public String getName() {
        return name;
    }
}
