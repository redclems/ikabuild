package fr.ikalogique.ikalist.model;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.ikalogique.ikalist.R;
import fr.ikalogique.ikalist.model.Build;

public class ListBuild extends AppCompatActivity {

    ArrayList<Build> mListe;

    public ListBuild()
    {
        mListe = new ArrayList<>();
        ajouteBuild("RyzeTest", new Champion(1, "Ryze"), Arrays.asList(new Item(1, "Rabadon")));
        ajouteBuild("NasusTest", new Champion(2, "Nasus"), Arrays.asList(new Item(2, "Luden")));
        ajouteBuild("LisandraTest", new Champion(3,"Lisandra"), Arrays.asList(new Item(3, "Zelda")));
    }

    public void ajouteBuild(@NonNull String titre, @NonNull Champion champ, @NonNull List<Item> listItem)
    {
        mListe.add(new Build(mListe.size(), titre, champ, listItem));
    }

    public Build get(int i) {
        return mListe.get(i);
    }

    public boolean deleteNote(int i) {
        if (i < 0 || i >= mListe.size()) return false;
        mListe.remove(i);
        return true;
    }

    public int deleteNote(@NonNull String nameBuild) {
        for (int i=0; i < mListe.size(); i++)
        {
            if (mListe.get(i).getName() == nameBuild)
            {
                mListe.remove(i);
                return i;
            }
        }
        return -1;
    }

    public int size() {
        return mListe.size();
    }
}

