package fr.ikalogique.ikalist.model;

import java.util.ArrayList;
import java.util.List;

public class Champion {

    private String name;


    private int id;
    private String pathImg;

    public Champion(int id, String name) {
        this.id = id;
        this.name = name;
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPathImg() {
        return pathImg;
    }
}
