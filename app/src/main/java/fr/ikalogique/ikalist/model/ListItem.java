package fr.ikalogique.ikalist.model;

import java.util.ArrayList;

public class ListItem {
    ArrayList<Item> mListe;

    public ListItem()
    {
        mListe = new ArrayList<>();

        mListe.add(new Item(1, "Rabadon"));
        mListe.add(new Item(2, "Nasus"));
        mListe.add(new Item(3, "Lysandra"));
    }


    public Item get(int i) {
        return mListe.get(i);
    }

    public int size() {
        return mListe.size();
    }
}

