package fr.ikalogique.ikalist.controller;

import fr.ikalogique.ikalist.model.Build;

public interface OnListInteractionListener {
    void onListInteraction(Build item);
}
