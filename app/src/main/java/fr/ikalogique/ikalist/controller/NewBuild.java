package fr.ikalogique.ikalist.controller;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fr.ikalogique.ikalist.R;
import fr.ikalogique.ikalist.model.Build;
import fr.ikalogique.ikalist.model.Champion;
import fr.ikalogique.ikalist.model.Item;

public class NewBuild extends AppCompatActivity {


    public static final String EXTRA_TITRE = "";
    public static final String EXTRA_CHAMP = "";
    public static final String EXTRA_CONTENU = "";

    MyChampionRecycleViewAdapter mAdapter;

    EditText mEditTitle;
    int idChamp = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_build);
        RecyclerView rView = findViewById(R.id.list);
        rView.setLayoutManager(new LinearLayoutManager(this));
        OnListInteractionChamp listeListener = new OnListInteractionChamp() {
            @Override
            public void onListInteraction(Champion champ) {
                idChamp = champ.getId();
                Toast toast = Toast.makeText(NewBuild.this, champ.getName() + " a été choisi", Toast.LENGTH_SHORT);
                toast.show();
            }

        };
        mEditTitle = findViewById(R.id.item_titre);

        mAdapter = new MyChampionRecycleViewAdapter(listeListener);
        rView.setAdapter(mAdapter);
    }


    public void validerBuild(View view) {
        String titre = mEditTitle.getText().toString();

        if(idChamp > -1 && !titre.equals("")) {

            Intent result = new Intent();

            result.putExtra(EXTRA_TITRE,titre);
            result.putExtra(EXTRA_CHAMP,idChamp +"");

            setResult(RESULT_OK, result);
            finish();
        }else {
            Toast toast = Toast.makeText(NewBuild.this, "titre incomplet ou champion non selectioner", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
