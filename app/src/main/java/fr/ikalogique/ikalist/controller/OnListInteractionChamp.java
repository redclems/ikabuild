package fr.ikalogique.ikalist.controller;

import fr.ikalogique.ikalist.model.Build;
import fr.ikalogique.ikalist.model.Champion;

public interface OnListInteractionChamp {
    void onListInteraction(Champion champ);
}
