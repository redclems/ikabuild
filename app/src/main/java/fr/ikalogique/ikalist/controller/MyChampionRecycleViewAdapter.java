package fr.ikalogique.ikalist.controller;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fr.ikalogique.ikalist.R;
import fr.ikalogique.ikalist.model.Build;
import fr.ikalogique.ikalist.model.Champion;
import fr.ikalogique.ikalist.model.Item;
import fr.ikalogique.ikalist.model.ListBuild;
import fr.ikalogique.ikalist.model.ListChampion;

public class MyChampionRecycleViewAdapter extends RecyclerView.Adapter<MyChampionRecycleViewAdapter.ViewHolder>{
    @NonNull

    private final ListChampion mChamp;
    private final OnListInteractionChamp mListener;

    public MyChampionRecycleViewAdapter(OnListInteractionChamp listener) {
        mChamp = new ListChampion();
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Champion noteAAfficher = mChamp.get(position);

        holder.majChampsTextes(noteAAfficher);

        // Sur chaque item créé par le recyclerview, on place un ClickListener qui va déclencher
        // notre callback maison.
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListInteraction(holder.mChamp);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mChamp.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public Champion mChamp;
        private TextView viewTitre;
        private TextView viewContenu;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            viewTitre = mView.findViewById(R.id.item_titre);
        }

        public void majChampsTextes(Champion build) {
            mChamp = build;
            viewTitre.setText(build.getName());
            /*
            String contenuAAfficher = build.getContenu();
            if (contenuAAfficher.length() > 50)
                contenuAAfficher = contenuAAfficher.substring(0,47)+"...";
            viewContenu.setText(contenuAAfficher);

             */
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }
}
