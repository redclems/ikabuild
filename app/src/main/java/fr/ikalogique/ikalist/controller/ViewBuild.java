package fr.ikalogique.ikalist.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import fr.ikalogique.ikalist.R;

public class ViewBuild extends AppCompatActivity {


    public static final String EXTRA_ID_BUILD = "0";
    EditText mEditTitre;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_build);
        String id = getIntent().getStringExtra(EXTRA_ID_BUILD);

        //mEditTitre = findViewById(R.id.persoName);
        //mEditTitre.setText(id);
    }

    public void retourBuild(View view) {
        Intent result = new Intent();

        setResult(RESULT_OK, result);
        finish();
    }
}