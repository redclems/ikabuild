package fr.ikalogique.ikalist.controller;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fr.ikalogique.ikalist.R;
import fr.ikalogique.ikalist.model.Build;
import fr.ikalogique.ikalist.model.Champion;
import fr.ikalogique.ikalist.model.Item;
import fr.ikalogique.ikalist.model.ListBuild;

public class MyBuildRecycleViewAdapter extends RecyclerView.Adapter<MyBuildRecycleViewAdapter.ViewHolder>{
    @NonNull

    private final ListBuild  mBuild;
    private final OnListInteractionListener mListener;

    public MyBuildRecycleViewAdapter(OnListInteractionListener listener) {
        mBuild = new ListBuild();
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_build, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Build noteAAfficher = mBuild.get(position);

        holder.majChampsTextes(noteAAfficher);

        // Sur chaque item créé par le recyclerview, on place un ClickListener qui va déclencher
        // notre callback maison.
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mBuild.size();
    }

    public void ajoutBuild(String titre, Champion champ, List<Item> contenu)
    {
        mBuild.ajouteBuild(titre, champ,contenu);
        notifyItemInserted(mBuild.size()-1);
    }

    public void supprimeNote(int index)
    {
        if (mBuild.deleteNote(index))
            notifyItemRemoved(index);
    }

    public void supprimeNote(String name)
    {
        int idRemove = mBuild.deleteNote(name);
        if (idRemove >= 0) notifyItemRemoved(idRemove);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public Build mItem;
        private TextView viewTitre;
        private TextView viewContenu;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            viewTitre = mView.findViewById(R.id.item_titre);
        }

        public void majChampsTextes(Build build) {
            mItem = build;
            viewTitre.setText(build.getName());
            /*
            String contenuAAfficher = build.getContenu();
            if (contenuAAfficher.length() > 50)
                contenuAAfficher = contenuAAfficher.substring(0,47)+"...";
            viewContenu.setText(contenuAAfficher);

             */
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }
}
