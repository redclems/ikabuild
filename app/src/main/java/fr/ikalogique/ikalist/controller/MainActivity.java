package fr.ikalogique.ikalist.controller;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import fr.ikalogique.ikalist.R;
import fr.ikalogique.ikalist.model.Champion;
import fr.ikalogique.ikalist.model.Item;

public class MainActivity extends AppCompatActivity {

    MyBuildRecycleViewAdapter mAdapter;

    public static final int REQUEST_CODE_INSERE_BUILD=42;
    public static final int REQUEST_CODE_VIEW_BUILD=43;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        RecyclerView rView = findViewById(R.id.list);
        rView.setLayoutManager(new LinearLayoutManager(this));
        OnListInteractionListener listeListener = new OnListInteractionListener() {
            @Override
            public void onListInteraction(fr.ikalogique.ikalist.model.Build item) {
                Intent viewBuildIntent = new Intent(MainActivity.this ,ViewBuild.class);
                viewBuildIntent.putExtra(ViewBuild.EXTRA_ID_BUILD,item.getName());
                startActivityForResult(viewBuildIntent,REQUEST_CODE_VIEW_BUILD);
            }

            public void effacer(){

            }
        };
        mAdapter = new MyBuildRecycleViewAdapter(listeListener);
        rView.setAdapter(mAdapter);
    }

    public void view_build(View view) {
        Intent insereNoteIntent = new Intent(this,NewBuild.class);
        startActivityForResult(insereNoteIntent,REQUEST_CODE_INSERE_BUILD);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==REQUEST_CODE_INSERE_BUILD) {
            String newTitre = data.getStringExtra(NewBuild.EXTRA_TITRE);
            String newChampID = data.getStringExtra(NewBuild.EXTRA_CHAMP);
            String newContenusID =  data.getStringExtra(NewBuild.EXTRA_CONTENU);
            // Ici, on peut vérifier que tout est OK dans les intents, et afficher une erreur s'il y a un soucis,
            // comme le titre ou le contenu à null, ce qui reflète un défaut dans le codage de l'activity appelée

            Champion newChamp = new Champion( Integer.parseInt(newChampID),"test");
            List<Item> newContenu = new ArrayList<>();

            List<Integer> idContenus = new ArrayList<>();
            String save = "";
            for(char c : newContenusID.toCharArray()){
                if(c == '1' || c == '2' || c == '3' || c == '4' || c == '5' || c == '6' || c == '7' || c == '8' || c == '9' || c == '0'){
                    save += c;
                }else if(c == ','){
                    idContenus.add(Integer.parseInt(save));
                    save = "";
                }
            }

            for(int i = 0; i < idContenus.size(); i++){
                newContenu.add(new Item(idContenus.get(i), "test"));
            }

            mAdapter.ajoutBuild(newTitre,  newChamp, newContenu);
        }

    }
}